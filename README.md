# Uptime Notifier

## Notifier

* Pings a server with the name of the computer in the `GET` url.

## Listener

* Listens for pings from the client.
