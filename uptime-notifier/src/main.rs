extern crate hyper;
extern crate hostname;

use std::env;

use hyper::rt::{self, Future};
use hyper::Client;
use hostname::get_hostname;
use std::thread::{spawn};

fn main() {
    // Some simple CLI args requirements...
    let url = env::args().skip(1).next().expect("Usage: client <url>");
    let url = format!("{}/{}", url, get_hostname().unwrap());

    println!("Url: {}", url);

    // HTTPS requires picking a TLS implementation, so give a better
    // warning if the user tries to request an 'https' URL.
    let url = url
        .parse::<hyper::Uri>()
        .expect(&format!("Couldn't parse URL: {}", url));
    if url.scheme_part().map(|s| s.as_ref()) != Some("http") {
        println!("This example only works with 'http' URLs.");
        return;
    }

    let mut handles = Vec::new();

    for _ in 0..10 {
        let url_clone = url.clone();
        let handle = spawn(move || {
            for _ in 0..500 {
                let url_clone = url_clone.clone();
                rt::run(fetch_url(url_clone));
            }
        });
        handles.push(handle);
    }

    for handle in handles {
        handle.join().unwrap();
    }

    // loop {
    //     let url_clone = url.clone();
    //     rt::run(fetch_url(url_clone));
    // }


    // Run the runtime with the future trying to fetch and print this URL.
    //
    // Note that in more complicated use cases, the runtime should probably
    // run on its own, and futures should just be spawned into it.
}

fn fetch_url(url: hyper::Uri) -> impl Future<Item = (), Error = ()> {
    // let https = HttpsConnector::new(4).unwrap();
    // let client = Client::builder().build::<_, hyper::Body>(https);
    let client = Client::new();

    client
        // Fetch the url...
        .get(url)
        /* // And then, if we get a response back...
        .and_then(|res| {
            println!("Response: {}", res.status());
            println!("Headers: {:#?}", res.headers());

            // The body is a stream, and for_each returns a new Future
            // when the stream is finished, and calls the closure on
            // each chunk of the body...
            res.into_body().for_each(|chunk| {
                io::stdout().write_all(&chunk)
                    .map_err(|e| panic!("example expects stdout is open, error={}", e))
            })
        }) */
        // If all good, just tell the user...
        .map(|_| {})
        // If there was an error, let the user know...
        .map_err(|err| {
            eprintln!("Error {}", err);
        })
}
