extern crate iron;
extern crate logger;
extern crate env_logger;

use iron::prelude::*;
use logger::Logger;

fn main() {
    let (logger_before, logger_after) = Logger::new(None);

    let mut chain = Chain::new(handler);

    // Link logger_before as your first before middleware.
    chain.link_before(logger_before);

    // Link logger_after as your *last* after middleware.
    chain.link_after(logger_after);

    Iron::new(chain).http("127.0.0.1:1234").unwrap();
}

fn handler(req: &mut Request) -> IronResult<Response> {
    println!("Requested: {}", req.url.path()[0]);
    println!("Requested: {}", req.remote_addr);
    Ok(Response::with(iron::status::Ok))
}
